class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def hello
      render html: "<h3>Hola Mundo!</h3>"
  end 
end
